$gameRunning = $true

class myMap {
    #$theMap = @("x", "x", "x", "x", "x", "x", "x", "x", "x")
    $theMap = @()
    $playerPos = 0
    $coordItem = @()

    $baseClass

    [int]$mapSize

    myMap([int]$mapSize, [int]$playerPos) {
        $this.baseClass = $true

        $this.updateVariablesFillMap($mapSize, $playerPos) 
    }
    myMap([int]$mapSize, [int]$playerPos, $inheritanceOrNot) {
        $this.baseClass = $inheritanceOrNot

        $this.updateVariablesFillMap($mapSize, $playerPos)
    }

    [void]updateVariablesFillMap([int]$mapSize, [int]$playerPos) {
        $this.mapSize = $mapSize
        $this.playerPos = $playerPos

        for ($i = 0; $i -lt $this.mapSize; $i++) {
            $this.theMap += "x"
        }
    }

    [void]updatePosition() {
        $this.updateMap($this.playerPos, "o")
    }

    [void]updateMap($indexOfVariable, $keyString) 
    {
        for ($i = 0; $i -lt $this.theMap.length; $i++) {
            if ($i -eq $indexOfVariable) {
                $this.theMap[$i] = $keyString
            }
        }
    }

    [void]erasePosition() {
        for ($i = 0; $i -lt $this.theMap.length; $i++) {
        if ($i -eq $this.playerPos) {
            $this.theMap[$i] = "x"
            }
        }
    }

    [int[]]moveDetailedController($newValue) {                            #Returns an array of error codes, the first for errorCode, the second if player
                                                                   #gained an item.
        if ($newValue -lt 0 -or $newValue -gt $this.mapSize - 1) {
            return @(-1,0)
        }

        $this.playerPos = $newValue
        return @(0,0)
    }

    [void]printMap() {
        $this.updatePosition()

        for ($i = 0; $i -lt $this.theMap.length; $i += 3) {
            Write-Host "$($this.theMap[$i]) $($this.theMap[$i + 1]) $($this.theMap[$i + 2])"  # "$($array[$i])" because then have space between chars.
        }
    }

    [int]moveController([string]$direction, [int]$amountMove) {
        $statusCode = @()
        $this.erasePosition()

        switch($direction){
        "w" {
            ($this.moveDetailedController($this.playerPos - $amountMove)).ForEach({$statusCode += $_})
            break 
            }
        "s" {
            ($this.moveDetailedController($this.playerPos + $amountMove)).ForEach({$statusCode += $_})
            break 
            }
        "a" {
            ($this.moveDetailedController($this.playerPos - 1)).ForEach({$statusCode += $_})
            break
            }
        "d" {
            ($this.moveDetailedController($this.playerPos + 1)).ForEach({$statusCode += $_})
            break
            }
        "e" {
            $statusCode = @(1,0)
            break
            }
        }

        return $statusCode[0]
    }

    [void]myMapController([string]$playerDirection, $amountMove, $inventory) {

        $statusCode = $this.moveController($playerDirection, $amountMove)
        switch($statusCode) {
            -1 {
                Clear-Host
                $this.printMap()
                Write-Host "You went outside of the map"
                break
                }
            0 {
                Clear-Host
                $this.printMap()
                break
               }
            1 {
                Write-Host "Inventory"
                $inventory.disp()
                break
               }
        }
    }

    [void]itemToMap($itemKey) {
        [System.Int32]$temp = $this.mapSize

        $randIndex = Get-Random -Maximum $temp

        $this.coordItem += , ($itemKey, $randIndex)       #add item to coordItem list
        $this.updateMap($randIndex, $itemKey)
    }
}

class largerMap : myMap {
    largerMap([int]$mapSize, [int]$playerPos) : base($mapSize, $playerPos, $false) {
    }

    [void]printMap() {
        ([myMap]$this).updatePosition()
        for ($i = 0; $i -lt $this.mapSize; $i += 6) {
            $string = ""

            for ($y = $i; $y -lt $i+6; $y++) {
                $string = $string + "$($this.theMap[$y]) "
            }
            Write-Host $string
        }
    }
}

class inventory {
    $itemList = @()

    inventory($refToMap) {
        $money = @("money", 0, "`$")
        $this.addItem($money, $refToMap)

    }

    [void]addItem($item, $refToMap) {
        $this.itemList += , ($item)        #I need the comma to actually add a new array inside itemList array.
        $this.itemToMap($this.itemList[$this.itemList.Length - 1][2], $refToMap)        #itemList[0][0] is the key of the first element in inventory
    }

    [void]disp() {
        for ($i = 0; $i -lt $this.itemList.Length; $i++) {

            Write-Host "$($this.ItemList[$i][0]) : $($this.ItemList[$i][1])"
        }
    }

    [void]itemToMap($itemKey, $refToMap) {
         ([myMap]$refToMap).itemToMap($itemKey)       #cast the reference into a base class, then calls the itemToMap function.
    }

}

$sizeMap = 9*4

$myMap = [largerMap]::new($sizeMap, 4)
#$myMap = [myMap]::new(9, 4)

$inventory = [inventory]::new($myMap)
$inventory.addItem(@("water bottle", 3, "W"), $myMap)

$myMap.printMap()

$amountMove = 0;

if ($myMap.baseClass -eq $true) {
        $amountMove = 3
    } 
else {
        $amountMove = 6
}

while($gameRunning) {

    $choice = Read-Host -Prompt "direction"
    
    $myMap.myMapController($choice, $amountMove, $inventory)

    if ($choice -eq "q") {exit }
}

 



